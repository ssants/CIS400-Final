#!/usr/bin/python3
from analysisTools import getAllTweets
import matplotlib
from matplotlib.ticker import FuncFormatter
import matplotlib.pyplot as plt
import numpy as np

def getTally(data):
    pos = 0
    neg = 0
    for e in data:
        if e.get('sentiment') == 'pos':
            pos += 1
        else:
            neg += 1
    
    return pos,neg

def makeFollowerBrackets(data):
    #l = []
    l1 = sorted(data, key = lambda i: i['followers'])
    brackets = []
    separator = 10
    while separator < 1000000:
        k = 0
        while l1[k].get("followers") < separator:
            k += 1
        brackets.append(l1[0:k])
        l1 = l1[k:]
        #separator += 1000
        separator *= 10
    brackets.append(l1)
    return brackets

def makeVerifiedBrackets(data):
    nonVerified = []
    verified = []
    for e in data:
        if e.get('verified'):
            verified.append(e)
        else:
            nonVerified.append(e)
    return nonVerified, verified

def makeApprovalPieChart(candidate, pos, neg):
    labels = 'Approve', 'Disapprove'
    sizes = [pos, neg]
    explode = (0.1, 0)

    _, ax = plt.subplots()
    ax.pie(x = sizes,
           explode = explode, 
           labels = labels,
           colors = ['g','r'],
           autopct = '%1.1f%%', 
           shadow = True, 
           startangle = 90)
    ax.axis('equal')
    ax.set(title = candidate + "'s Approval Rating")

    plt.show()

def makeBarApprovalChart(data):
    n_groups = len(data)

    #means_men = (20, 35, 30, 35, 27)
    #means_women = (25, 32, 34, 20, 25)
    approval = [a for (c,a,d) in data]
    disapproval = [d for (c,a,d) in data]

    fig, ax = plt.subplots()

    index = np.arange(n_groups)
    bar_width = 0.35

    opacity = 0.4   
    error_config = {'ecolor': '0.3'}

    rects1 = ax.bar(x = index, 
                    height = approval, 
                    width = bar_width,
                    alpha=opacity, 
                    color='g',
                    error_kw=error_config,
                    label='Approval')

    rects2 = ax.bar(x = index + bar_width, 
                    height = disapproval, 
                    width = bar_width,
                    alpha=opacity, 
                    color='r',
                    error_kw=error_config,
                    label='Disapproval')

    ax.set_xlabel('Candidates')
    ax.set_ylabel('Ratings')
    ax.set_title('Direct Approval Ratings from Twitter')
    ax.set_xticks(index + bar_width / 2)
    
    labels = [c for (c,a,d) in data]
    ax.set_xticklabels(labels)
    ax.legend()

    fig.tight_layout()
    plt.show()

def makeFollowerBracketsNum(brackets):
    data = [len(a) for a in brackets]
    n_groups = len(data)

    fig, ax = plt.subplots()

    index = np.arange(n_groups)
    bar_width = 0.35

    opacity = 0.4   
    error_config = {'ecolor': '0.3'}

    ax.bar(x = index,
           height = data,
           width = bar_width,
           alpha=opacity,
           color='g',
           error_kw=error_config,
           label='Approval')

    ax.set_xlabel('Brackets')
    ax.set_ylabel('Population')
    ax.set_title('Population of each bracket of Followers')
    ax.set_xticks(index)
    
    i = [10,100,1000,10000,100000,1000000]
    labels = [str(j) for j in i]
    ax.set_xticklabels(labels)
    #ax.legend()

    fig.tight_layout()
    plt.show()

def findSentimentOfBrackets(brackets):
    data = []
    for a in brackets:
        pos, neg = getTally(a)
        data.append((pos/(pos+neg)))
    return data

def makeFollowerBracketsApproval(brackets):
    data = findSentimentOfBrackets(brackets)
    n_groups = len(data)

    fig, ax = plt.subplots()

    index = np.arange(n_groups)
    bar_width = 0.35

    opacity = 0.4   
    error_config = {'ecolor': '0.3'}

    ax.bar(x = index,
           height = data,
           width = bar_width,
           alpha=opacity,
           color='g',
           error_kw=error_config,
           label='Approval')

    ax.set_xlabel('Brackets')
    ax.set_ylabel('Ratings')
    ax.set_title('Direct Approval Ratings from Twitter per bracket')
    ax.set_xticks(index)
    
    i = [10,100,1000,10000,100000,1000000]
    labels = [str(j) for j in i]
    ax.set_xticklabels(labels)
    #ax.legend()

    fig.tight_layout()
    plt.show()

def makeVerifiedApproval(data):
    nonVerified, verified = makeVerifiedBrackets(data)

    posN, negN = getTally(nonVerified)
    posV, negV = getTally(verified)
    data1 = [posN/(posN+negN),posV/(posV+negV)]
    data2 = [negN/(posN+negN),negV/(posV+negV)]

    n_groups = 2

    fig, ax = plt.subplots()

    index = np.arange(n_groups)
    bar_width = 0.35

    opacity = 0.4   
    error_config = {'ecolor': '0.3'}

    rects1 = ax.bar(x = index, 
                    height = data1, 
                    width = bar_width,
                    alpha=opacity, 
                    color='g',
                    error_kw=error_config,
                    label='Approval')

    rects2 = ax.bar(x = index + bar_width, 
                    height = data2, 
                    width = bar_width,
                    alpha=opacity, 
                    color='r',
                    error_kw=error_config,
                    label='Disapproval')

    ax.set_xlabel('Users')
    ax.set_ylabel('Ratings')
    ax.set_title('Direct Approval Ratings from Twitter')
    ax.set_xticks(index + bar_width / 2)
    
    ax.set_xticklabels(['Unverified','Verified'])
    ax.legend()

    fig.tight_layout()
    plt.show()

if __name__ == "__main__":
    # data = getAllTweets("biden.txt")
    # data = Sent_Analysis_2(data)
    # #makeVerifiedApproval(data)
    # e = makeFollowerBrackets(data)
    # makeFollowerBracketsNum(e)
    # makeFollowerBracketsApproval(e)
    # pos, neg = getTally(data)
    pos, neg = .2 , .8
    makeApprovalPieChart("Bernie", pos, neg)
    # a = [("Bernie", .2, .8),("Trump", .7, .3),("Biden", .4, .6)]
    # makeBarApprovalChart(a)

    # 
    # total = pos + neg

    # print(pos/total)
    # print(neg/total)

    # print(str(len(data)) + "\n")
    # total = len(data)
    # l = []
    # l1 = sorted(data, key = lambda i: i['followers'])
    
    # brackets = []
    # separator = 100
    # while separator < 1000000:
    #     j = 0
    #     k = 0
    #     while l1[k].get("followers") < separator:
    #         l.append(l1[k])
    #         j += 1
    #         k += 1
    #     brackets.append(l1[0:j])
    #     l1 = l1[j:]
    #     separator *= 10

    # s = 100
    # for b in brackets:
    #     print("Followers less than: " + str(s))
    #     print(str(len(b)) + "\n")
    #     s *= 10
