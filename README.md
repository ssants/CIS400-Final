# Predicting the Outcome of the 2020 Primaries using Twitter
We are Jay, Aiden, Alan, Tim and Sergio, and this is our final project for CIS 400: Principles of Social Media and Mining (Spring 2019)

We try to predict who will win the Democratic primaries by conducting a sentiment analysis on tweets related to the top candidates. 
While we originally wanted to do this for both parties, the Republican pool was too small (with only the current president, Donald Trump, and one other candidate).
We also conducted a centrality analysis to determine influential Twitter accounts in the political landscape, 
with the hope of using their centrality as a weight during the sentiment analysis.

#### Installation
Our three main programs, `miner.py`, `sentiment_analysis.py` and `centrality.py` make use of a wide variety of Python libraries:

* `tweepy`, an easy-to-use wrapper for the Twitter API
* `twitter`, a minimalist wrapper for the Twitter API
* `networkx`, a library for creating, manipulating and analyzing a wide variety of graphs
* `matplotlib`, a plotting package
* `nltk`, the natural language processing toolkit
* `textblob`, a simpler API for processing text that plays nicely with `nltk`
* optional: `blessings`, a library that make working in terminals easier

To use `tweepy` and `twitter` you need to create your own 
[Twitter Developer](https://developer.twitter.com/en/apps)
account and application to get a consumer key and secret and access token and secret. 

Additionally, `nltk` needs to download some files, so fire up the Python interpreter and run:

```python
import nltk
nltk.download('twitter_samples')
nttk.download('stopwords')
```

You are now ready to run any of our programs.
Read further for mor details on each.

## Mining Tweets with `miner.py`
During our mining we decided to focus on the top 5 candidates for the Democratic primaries and relevant hashtags for each:

| List        | Candidate        | Keywords |
| ----------- | ---------------- | -------- |
| `bidenTerms`  | Joe Biden        | joebiden, teamjoe, biden2020, joe2020 |
| `bernieTerms` | Bernie Sanders   | sensanders, berniesanders, feelthebern, bernie2020 |
| `warrenTerms` | Elizabeth Warren | senwarren, ewarren, winwithwarren, warren2020 |
| `peteTerms`   | Pete Buttigleg   | petebuttigleg, buttigleg, pete2020, buttigleg2020 |
| `harrisTerms` | Kamala Harris    | kamalaharris, harris2020, kamala2020 |

Once installed, make sure to insert your API keys. Now the program can be called:

```python
import miner
# miner.main(terms, filename)
miner.main(bernieTerms, 'bernie.bin')
```

where `terms` is a list of words to track and `filename` is a file to output to.
The miner will run indefinitely, so sit back and relax.

## Centrality Analysis with `centrality.py`
The `centrality.py` program has a couple of constants defined near the top, including
`SEED`, the user name to start at, and a couple limits like `NODE_LIMIT`, the max number
of nodes to include and `MAX_DEPTH`, the number of levels to traverse.
There is also the `MODE` constant that should either be 
`'crawl'` to get a network from a user's followers or
`'create'` to make a network from a user's friends (who they follow).
The default mode is `'crawl'` with a `MAX_DEPTH` of 3.

There are a few functions that do separate things:

* `create_network(seed)`: create a bottom-up network starting from user `seed` by adding friends recursively
* `crawl_network(seed)`: get a top-down network starting from user `seed` by adding followers recursively
* `analyze_network(g)`: run some analysis (such as Katz centrality) on a `networkx` graph
* `get_centrality_score(user,g)`: returns the Katz centrality of a user given their network
* `graph_network(g)`: graph a given network using `matplotlib`

You can also just call `main` to run the program automatically with the defined constants.

One final note: the graph of the network is stored as a `dict` of `list` into `nodes.bin`
and an image of the network is saved as `network.png` for use later.

```python
import centrality
# Either use
centrality.main()
# or use
centrality.create_network(seed='realdonaldtrump')
```

## Sentiment Analysis with `sentiment_analysis.py`
For sentiment analysis the large majority of the code was to set up the classifier.
This included tokenizing the tweet, removing stop words, and normalizing/lemmatizing each word.
All of this is put togethere in the Sent_Analysis method, which returns a tuple of `int`s:

```python
return (counter_sent_neg, counter_sent_pos)
```

where `counter_sent_neg` is the number of negative tweets in the data set and `count_sent_pos` is
the number of positive tweets in the data set.

The simplest way to use `sentiment_analysis.py` is to edit the `FILENAME` variable at the top to
point to the data set to analyze. Then just run:

```python
import sentiment_analysis
sentiment_analysis.main()
```

## Misc
The `utility.py` and `analysisTools.py` files are different collections of functions to 
aid different programs. The `utility.py` program pairs with `centrality.py` and has most of the 
API interacting functions and a class for modeling Twitter users. The `analysisTools.py` program 
helps bridge the gap between `miner.py` and `sentiment_anlysis.py` by reading the data sets
mined by the miner into the sentiment analyzer.

The final program, `displayer.py` was used to create and display graphs using the final 
sentiment score. It also includes the option to run sentiment analysis if not done yet
and then create graphs.

