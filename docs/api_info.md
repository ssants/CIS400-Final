# Using the [`twitter` Library](https://github.com/sixohsix/twitter)
The `twitter` library differs from the `tweepy` library in that a 
function such as `api.users.show()` will always point to the matching URL 
(in this case <http://twitter.com/users/show>), even if Twitter changes
the actual endpoint.
Additionally, `twitter` returns responses as dictionaries, whereas 
`tweepy` has custom classes for common objects like users and tweets.


In this project we use the `twitter` library for in `centrality.py`, but this will 
probably switch over to `tweepy` in the future.
The two main functions used in `centrality.py` are the
["friends/list"](https://developer.twitter.com/en/docs/accounts-and-users/follow-search-get-users/api-reference/get-friends-list)
and 
["friendships/show"](https://developer.twitter.com/en/docs/accounts-and-users/follow-search-get-users/api-reference/get-friendships-show)
endpoints.
"friends/list" was chosen over 
["friends/ids"](https://developer.twitter.com/en/docs/accounts-and-users/follow-search-get-users/api-reference/get-friends-ids)
because the latter required an additional call to
["users/show"](https://developer.twitter.com/en/docs/accounts-and-users/follow-search-get-users/api-reference/get-users-show)
to get user information where as the former did both at once.

# Endpoints
Note:
- All parameters are optional unless otherwise noted.
- Boolean types are written as "true" or "false" instead of the typical Python way.

## friends/list
The `friends/list` endpoint returns a cursored list of friends of the specified user.
It returns up to 200 friends per page and provides a `cursor` for retrieving 
previous or next pages. 
The `friends/list` endpoint requires authentication (using `twitter.Twitter` with auth)
and is limited to 15 requests per 15 minutes. 

#### Parameters

| Parameter | Type  | Description |
| ---- | --- | --- |
| `user_id`               | `int`  | user id to return results for (or use `screen_name`) |
| `screen_name`           | `str`  | user name to return results for (or use `user_id`)|
| `cursor`                | `int`  | cursor for page to get. -1 for first page, 0 for last |
| `count`                 | `int`  | number of users to return to page (default is 20) |
| `skip_status`           | `bool` | whether or not to return status for users | 
| `include_user_entities` | `bool` | whether or not to include entities obj for users |

#### Return Value
Returns a dictionary of items including:

* `next_cursor`: `int`, cursors for next page.
* `previous_cursor`: `int`, cursor for previous page
* `users`: `list`, List of `User` objects for friends of the specified user

## followers/list
The `followers/list` endpoint returns a cursored list of followers of the specified user.
It returns up to 200 followers per page and provides a `cursor` for retrieving
previous or next pages.
The `followers/list` endpoint requires authentication (using `twitter.Twitter` with auth)
and is limited to 15 request per 15 minutes.

#### Parameters
Same parameters as `friends/list`, check that out

#### Return Value
Returns a dictionary of items including:

* `next_cursor`: (`int`) cursor for the next page
* `previous_cursor`: (`int`) cursor for the previous page
* `users`: (`list`), list of `User` objects for followers of the specified user

## friendships/show
The `friends/show` endpoint details the relationship between two users,
including if they follow each other or if they can DM each other.
It requires authentication (using `twitter.Twitter` with auth)
and is limited to 180 requests per 15 minutes. 

#### Parameters

| Parameter | Type | Description |
| --- | --- | --- |
| `source_id`          | `int` | first user to check (using id) |
| `source_screen_name` | `str` | first user to check (using user name) |
| `target_id`          | `int` | second user to check (using id) |
| `target_screen_name` | `str` | second user to check (using user name) |

#### Return Value
Returns a dictionary of values including:

* `relationship`: `dict`, the root object. Its value is a relationship dictionary.
* `source` and `target`: `dict`, the two dictionaries at the root of `relationship`. Each contains:
	* `following`: `bool`, whether this user follows the other
	* `followed_by`: `bool`, whether the other user follows this user

The `source` dictionary also includes some values that `target` doesn't such as:

* `can_dm`: `bool`, whether the source user can DM the target user

## Twitter Objects

### User object
The dictionary of user information includes values such as:
* `id`: `int`, the user's unique ID number
* `screen_name`: `str`, the user's Twitter handle, e.g. @Twitter
* `followers_count`: `int`, number of people following the user
* `friends_count`: `int` number of people the user is following
* `verified`: `bool`, true if user is verified
* `statuses_count`: `int`, number of tweets (including re-tweets) by the user
* `created_at`: `str`, the date this account was created

